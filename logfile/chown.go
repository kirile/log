// +build !linux

package logfile

import (
	"os"
)

func chown(_ string, _ os.FileInfo) error {
	return nil
}
