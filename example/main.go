package main

import (
	"gitee.com/kirile/log"
	"time"
)

func main() {

	l,err := log.NewLogger(log.Option{
		ConsoleLevel: log.FATAL,
		FileLevel:    log.DEBUG,
		LogDir:       "log", //will create directory if not exist
		Prefix:       "app", // will write "[app]" prefix
		IsLib:        false,  // normal use set it to false
		Zip:          false,  // compress log file when rotate
		MaxMegaBytes: 0,    // the unit is MB
		MaxCount:     0,   // default is 5
		MaxAge:       0,   // default 7 days
	})

	if err!=nil {
		panic(err)
		return
	}

	for i:=0;i<100000 ;i++  {
		l.Debug("debug string")
		l.Info("info string")
		l.Warn("warn string")
		l.Error("error string")
		time.Sleep(time.Second)
		//l.Fatal("fatal string")
	}
}
