# log

自用日志库

## 功能

- 基于go自带的log, 小幅修改了日志输出格式
- 以天为单位自动分文件写出日志
- 终端日志颜色输出, ~~使用了 mattn 的库~~ 颜色库合并了一下放在`gitee.com/kirile/color`, 同时也支持windows 以及 VSCODE和Goland的调试时的那个终端
- 输出文件行数
- github和golang.org的包全部整合了

## 其他


## 使用方法

```go
import "gitee.com/kirile/log"

l,err := log.NewLogger(log.Option{
		ConsoleLevel: log.FATAL,
		FileLevel:    log.DEBUG,
		LogDir:       "log", //will create directory if not exist
		Prefix:       "app", // will write "[app]" prefix
		IsLib:        false,  // normal use set it to false
		Zip:          false,  // compress log file when rotate
		MaxMegaBytes: 0,    // the unit is MB
		MaxCount:     0,   // default is 5
		MaxAge:       0,   // default 7 days
	})

```

跨日自动备份并进入新文件
